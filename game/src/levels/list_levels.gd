extends Node

const levels = {
	"Level 1": "res://game/src/levels/level_1.tscn",
	"Test": "res://game/src/levels/test_level.tscn",
	"Wall Jump": "res://game/src/levels/wall_jump_level.tscn",
	"Wave Dash": "res://game/src/levels/wave_dash_level.tscn",
}
