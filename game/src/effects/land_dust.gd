extends Effect


func _set_offset():
	offset.y -= sprite_frames.get_frame_texture("animation", frame).get_height()
	offset.x -= sprite_frames.get_frame_texture("animation", frame).get_width() \
			/ 2.0
