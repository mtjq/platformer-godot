class_name Effect
extends AnimatedSprite2D


var anim_name = "animation"


func _ready():
	connect("animation_finished", _on_animation_finished)
	frame = 0
	_set_offset()
	play(anim_name)


func _set_offset():
	pass


func _on_animation_finished():
	queue_free()
