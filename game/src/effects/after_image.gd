extends Node2D


func _ready():
	$Countdown.start()


func _process(_delta):
	var r = pow(
			float($Countdown.get_frames_left()) \
					/ float($Countdown.get_total_frames()),
			2.0
	) * 0.8
	$Sprite.material.set_shader_parameter("r", r)


func set_texture(text, flip):
	$Sprite.set_texture(text)
	$Sprite.position.y = - text.get_height() / 2.0
	$Sprite.flip_h = flip


func set_starting_material(c, base_r):
	$Sprite.material.set_shader_parameter("c", c)
	$Sprite.material.set_shader_parameter("base_r", base_r)


func _on_countdown_over():
	queue_free()

