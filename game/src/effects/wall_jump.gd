extends Effect


func set_anim(is_vertical = false):
	anim_name = "vertical" if is_vertical else "diagonal"


func _set_offset():
	var off_dir = 1 if flip_h else -1
	offset.x += off_dir \
			* sprite_frames.get_frame_texture(anim_name, frame).get_width() \
			/ 2.0
