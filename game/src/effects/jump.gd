extends Effect


func set_anim(
		input_dir_x,
		player_dir_h,
		is_double_jump = false,
		is_wave_dash = false
):
	if is_double_jump:
		anim_name = "double_jump"
		flip_h = player_dir_h == -1
	elif is_wave_dash:
		if input_dir_x:
			anim_name = "wave_dash"
			flip_h = input_dir_x == -1
		else:
			anim_name = "wave_dash_vertical"
			flip_h = player_dir_h == -1
	else:
		if input_dir_x:
			anim_name = "diagonal"
			flip_h = input_dir_x == -1
		else:
			anim_name = "vertical"
			flip_h = player_dir_h == -1


func _set_offset():
	if anim_name != "double_jump":
		offset.y -= sprite_frames.get_frame_texture(anim_name, frame).get_height() \
				/ 2.0
