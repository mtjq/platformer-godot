extends Node


signal over

@export var frames = 60
@export var autostart = false
@export var loop = false

var _stopped = true
var _paused = true
var _frames_left = 0


func _ready():
	if autostart:
		_frames_left = frames
		_stopped = false
		_paused = false


func _physics_process(_delta):
	if _frames_left == 0 or _paused:
		return
	
	_frames_left -= 1
	if _frames_left > 0:
		return
	
	if loop:
		_frames_left = frames
	else:
		_stopped = true
		_paused = true
	emit_signal("over")


func add_frames(f):
	if _frames_left > 0:
		_frames_left += f


func pause():
	_paused = true


func is_paused():
	return _paused


func resume():
	_paused = false


func stop():
	_frames_left = 0
	_stopped = true
	_paused = true


func is_stopped():
	return _stopped


func start(new_frames = frames):
	_frames_left = new_frames
	if not Engine.is_in_physics_frame():
		_frames_left += 1
	_stopped = false
	_paused = false


func get_frames_left():
	return _frames_left


func get_total_frames():
	return frames
