extends MarginContainer


var LevelsMenu = preload("res://game/src/menus/levels_menu.tscn")


func _ready():
	$Home/PlayDefault.grab_focus()


func _on_play_default_pressed():
	get_tree().change_scene_to_file(Levels.levels["Level 1"])


func _on_select_level_pressed():
	$Home.hide()
	$Home.process_mode = PROCESS_MODE_DISABLED
	var levels_menu = LevelsMenu.instantiate()
	add_child(levels_menu)
	levels_menu.exit.connect(_back_home.bind(levels_menu))


func _back_home(menu):
	menu.queue_free()
	$Home.show()
	$Home.process_mode = PROCESS_MODE_INHERIT
	$Home/PlayDefault.grab_focus()


func _on_quit_pressed():
	get_tree().quit()
