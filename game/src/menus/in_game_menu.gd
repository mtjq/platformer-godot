class_name InGameMenu
extends ColorRect


enum Kind {PAUSE, DEATH}

var LevelsMenu = preload("res://game/src/menus/levels_menu.tscn")


@onready var container = find_child("MarginContainer")
@onready var home = find_child("Home")
@onready var label = find_child("Label")
@onready var first_button = find_child("FirstButton")
@onready var button_select_level = find_child("SelectLevel")
@onready var button_quit = find_child("Quit")


func _ready():
	button_select_level.pressed.connect(_select_level)
	button_quit.pressed.connect(get_tree().quit)
	hide()


func open(kind = Kind.PAUSE):
	get_tree().paused = true
	show()
	if kind == Kind.PAUSE:
		first_button.text = "Resume"
		first_button.pressed.connect(_unpause)
		label.text = "Paused"
	elif kind == Kind.DEATH:
		first_button.text = "Restart"
		first_button.pressed.connect(_restart)
		label.text = "Nice Try"
	first_button.grab_focus()


func _close():
	get_tree().paused = false
	hide()
	for c in first_button.pressed.get_connections():
		first_button.pressed.disconnect(c.callable)


func _unpause():
	_close()


func _restart():
	_close()
	get_tree().reload_current_scene()


func _select_level():
	home.hide()
	home.process_mode = PROCESS_MODE_DISABLED
	var levels_menu = LevelsMenu.instantiate()
	container.add_child(levels_menu)
	levels_menu.exit.connect(_back_home.bind(levels_menu))


func _back_home(menu):
	menu.queue_free()
	home.show()
	home.process_mode = PROCESS_MODE_WHEN_PAUSED
	first_button.grab_focus()
