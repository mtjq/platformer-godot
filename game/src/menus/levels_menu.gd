extends VBoxContainer


signal exit

var levels_dict = Levels.levels
var buttons = []


func _ready():
	for level in levels_dict:
		var button = Button.new()
		button.text = level
		button.pressed.connect(_on_button_pressed.bind(levels_dict[level]))
		buttons.append(button)
		add_child(button)
	buttons[0].grab_focus()
	buttons[0].focus_neighbor_top = buttons[-1].get_path()
	buttons[-1].focus_neighbor_bottom = buttons[0].get_path()


func _input(event):
	if event.is_action_pressed("ui_cancel"):
		exit.emit()


func _on_button_pressed(level):
	get_tree().paused = false
	get_tree().change_scene_to_file(level)
