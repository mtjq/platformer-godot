extends Node2D


## The detectors watch along it's owner's movement for the next frame. This
## is the movement's multiplier to have a safe margin and be sure to detect.
@export var margin = 1.1

var detect_dict


func _ready():
	detect_dict = {
		$CornerDetectorTL: Vector2(-1, -1),
		$CornerDetectorTR: Vector2(1, -1),
		$CornerDetectorBL: Vector2(-1, 1),
		$CornerDetectorBR: Vector2(1, 1),
	}


func disable():
	for c in detect_dict:
		c.enabled = false


func update_detectors(next_move):
	var cast_target = next_move * margin
	for d in detect_dict:
		if d.enabled:
			d.target_position = cast_target


func get_detector_colliding():
	var detectors = []
	for detector in detect_dict:
		if detector.is_colliding():
			detectors.append(detector)
	return null if detectors.size() != 1 else detectors[0]


func get_corner_dir_pos():
	var detect_colliding = get_detector_colliding()
	if detect_colliding == null:
		return null
	var corner_dir = detect_dict[detect_colliding]
	var tilemap = detect_colliding.get_collider(0)
	var tile_size = Vector2(tilemap.tile_set.tile_size)
	var col_rid = detect_colliding.get_collider_rid(0)
	var tile_idx = tilemap.get_coords_for_body_rid(col_rid)
	var tile_pos = tilemap.to_global(tilemap.map_to_local(tile_idx))
	var corner_pos = tile_pos - 0.5 * corner_dir * tile_size
	return [corner_dir, corner_pos]
