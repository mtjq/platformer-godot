extends Node2D


@onready var r = $WallDetectorRight
@onready var l = $WallDetectorLeft


func is_detecting():
	return r.is_colliding() or l.is_colliding()


func get_wall_dir():
	return (int(r.is_colliding()) - int(l.is_colliding()))


func get_contact_x():
	if r.is_colliding() and l.is_colliding():
		return owner.position.x
	var d = r if r.is_colliding() else l
	var dir = 1 if r.is_colliding() else -1
	var tilemap = d.get_collider(0)
	var tile_size = Vector2(tilemap.tile_set.tile_size)
	var col_rid = d.get_collider_rid(0)
	var tile_idx = tilemap.get_coords_for_body_rid(col_rid)
	var tile_pos = tilemap.to_global(tilemap.map_to_local(tile_idx))
	return (tile_pos.x - 0.5 * dir * tile_size.x)
