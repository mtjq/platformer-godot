extends Area2D


var shapes = []


func _ready():
	for c in get_children():
		if c is CollisionShape2D:
			shapes.append(c)


func set_dir(dir):
	for shape in shapes:
		shape.position.x = dir * abs(shape.position.x)


func get_dir():
	return sign(shapes[0].position.x)
