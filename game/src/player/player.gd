class_name Player
extends CharacterBody2D


@export_group("Toggles")
@export var grump_enabled = false
## When dashing and colliding diagonally with a surface, keeps total speed
## instead of cutting the normal component.
@export var perfect_dash_slide = true
@export_group("Basics")
@export var JUMP_HEIGHT = 42.0:
	set(val):
		JUMP_HEIGHT = val
		_compute_player_physics()
@export var JUMP_DURATION = 0.3:
	set(val):
		JUMP_DURATION = val
		_compute_player_physics()
@export var FALL_DURATION = 0.26:
	set(val):
		FALL_DURATION = val
		_compute_player_physics()
@export var MAX_SPEED_H = 155.0:
	set(val):
		MAX_SPEED_H = val
		_compute_player_physics()
@export_subgroup("Misc")
@export var ACCELERATION_TO_MAX_SPEED_H = 20.0:
	set(val):
		ACCELERATION_TO_MAX_SPEED_H = val
		_compute_player_physics()
@export var MAX_FALL_SPEED_TO_JUMP_IMPULSE = 1.2:
	set(val):
		MAX_FALL_SPEED_TO_JUMP_IMPULSE = val
		_compute_player_physics()
@export var MAX_WALL_FALL_SPEED_TO_MAX_FALL_SPEED = 0.25:
	set(val):
		MAX_WALL_FALL_SPEED_TO_MAX_FALL_SPEED = val
		_compute_player_physics()
## Used a few frames after a wall jump so we can't go back to the wall.
@export var LOW_ACCELERATION_TO_ACCELERATION = 0.55:
	set(val):
		LOW_ACCELERATION_TO_ACCELERATION = val
		_compute_player_physics()
@export_group("Wall jump")
@export var WALL_JUMP_VELOCITY_TO_JUMP_IMPULSE = Vector2(0.65, - 0.6):
	set(val):
		WALL_JUMP_VELOCITY_TO_JUMP_IMPULSE = val
		_compute_player_physics()
@export var NEUTRAL_WALL_JUMP_VELOCITY_TO_JUMP_IMPULSE = Vector2(0.4, - 1.05):
	set(val):
		NEUTRAL_WALL_JUMP_VELOCITY_TO_JUMP_IMPULSE = val
		_compute_player_physics()
@export var WALL_GRAB_JUMP_VELOCITY_TO_JUMP_IMPULSE = Vector2(0.0, - 1):
	set(val):
		WALL_GRAB_JUMP_VELOCITY_TO_JUMP_IMPULSE = val
		_compute_player_physics()
@export_group("Dash")
@export var DASH_SPEED_MULTIPLIER = 1.1
@export var DASH_MIN_SPEED_TO_JUMP_IMPULSE = 1.3:
	set(val):
		DASH_MIN_SPEED_TO_JUMP_IMPULSE = val
		_compute_player_physics()
@export var WAVE_DASH_SPEED_MULTIPLIER = 1.0
## When performing a regular wave dash on floor, the vertical impulse compared
## to the regular jump impulse.
@export var WAVE_DASH_VERTICAL_IMPULSE_TO_JUMP_IMPULSE = 0.7:
	set(val):
		WAVE_DASH_VERTICAL_IMPULSE_TO_JUMP_IMPULSE = val
		_compute_player_physics()
@export var NEUTRAL_WAVE_DASH_VECTOR = Vector2(0, -1.1)
@export var WALL_WAVE_DASH_VECTOR = Vector2(1, -0.7)
@export var NEUTRAL_WALL_WAVE_DASH_VECTOR = Vector2(0.4, -1.1)
@export var SURFACE_SLIDE_ADDITIONAL_FRAMES = 2
@export_group("Grump")
@export var GRUMP_IMPULSE_TO_JUMP_IMPULSE = 1.0:
	set(val):
		GRUMP_IMPULSE_TO_JUMP_IMPULSE = val
		_compute_player_physics()
@export var GRUMP_SPEED_MULT_MIN = 0.9
@export var GRUMP_SPEED_MULT_MAX = 1.1

var JumpEffect = preload("res://game/src/effects/jump.tscn")
var RunDust = preload("res://game/src/effects/run_dust.tscn")
var LandDust = preload("res://game/src/effects/land_dust.tscn")
var WallJumpEffect = preload("res://game/src/effects/wall_jump.tscn")
var GRAVITY_UP
var GRAVITY_DOWN
var JUMP_IMPULSE
var WALL_JUMP_VELOCITY
var NEUTRAL_WALL_JUMP_VELOCITY
var WALL_GRAB_JUMP_VELOCITY
var MAX_FALL_SPEED
var MAX_WALL_FALL_SPEED
var ACCELERATION
var LOW_ACCELERATION
var DASH_MIN_SPEED
var WAVE_DASH_VERTICAL_IMPULSE
var GRUMP_IMPULSE
var player_size = Vector2(8, 13)
var direction_h = 1
var wall_jump_dir = 0
var wall_jump_pos = Vector2.ZERO
var corner_dodge_offset_margin = 0.001

@onready var menu = $InGameMenu
@onready var sm = $StateMachine


func _ready():
	_compute_player_physics()
	$HurtBox.body_entered.connect(_death)
	if not grump_enabled:
		$GrumpCornerDetectors.disable()
	sm.init()


func _input(event):
	if event.is_action_pressed("pause"):
		menu.open(menu.Kind.PAUSE)
	else:
		$StateMachine.sm_input(event)


func _unhandled_input(event):
	sm.sm_unhandled_input(event)


func _process(delta):
	sm.sm_process(delta)


func _physics_process(delta):
	sm.sm_physics_process(delta)
	if sm.state.name == "Dash":
		_dash_move_and_slide(delta)
	else:
		move_and_slide()


func get_central_pos():
	return (position - Vector2(0, float(player_size.y) / 2.0))


func set_direction_h(dir):
	direction_h = dir
	$GrabArea.set_dir(dir)
	if direction_h > 0:
		$Sprite.flip_h = false
	elif direction_h < 0:
		$Sprite.flip_h = true


func is_on_surface():
	return (is_on_floor()
			or find_child("WallDetectors").is_detecting()
			or is_on_ceiling()
	)


func create_jump_effect(
		input_dir_x,
		is_double_jump = false,
		is_wave_dash = false
):
	var jump_effect = JumpEffect.instantiate()
	jump_effect.position = position
	jump_effect.set_anim(
			input_dir_x,
			direction_h,
			is_double_jump,
			is_wave_dash
	)
	get_tree().current_scene.add_child(jump_effect)


func create_wall_jump_effect(is_vertical):
	var wall_jump_effect = WallJumpEffect.instantiate()
	wall_jump_effect.position = wall_jump_pos
	wall_jump_effect.flip_h = wall_jump_dir > 0
	wall_jump_effect.set_anim(is_vertical)
	get_tree().current_scene.add_child(wall_jump_effect)


func create_land_dust():
	if not find_child("DustCD").is_stopped():
		return
	$Countdowns/DustCD.start()

	var land_dust = LandDust.instantiate()
	land_dust.position = position
	get_tree().current_scene.add_child(land_dust)


func create_run_dust():
	if not find_child("DustCD").is_stopped():
		return
	
	var run_dust = RunDust.instantiate()
	run_dust.position = position
	if $StateMachine.input_dir.x < 0:
		run_dust.flip_h = true
	get_tree().current_scene.add_child(run_dust)


func get_current_texture():
	var image = $Sprite.texture.get_image()
	var image_size = image.get_size()
	var frame_size = Vector2(image_size.x / $Sprite.hframes, image_size.y / $Sprite.vframes)
	var crop = Rect2(Vector2($Sprite.frame_coords) * frame_size, frame_size)
	return ImageTexture.create_from_image(image.get_region(crop))


func _compute_player_physics():
	GRAVITY_UP = 2 * JUMP_HEIGHT / (JUMP_DURATION**2)
	GRAVITY_DOWN = 2 * JUMP_HEIGHT / (FALL_DURATION**2)
	JUMP_IMPULSE = 2 * JUMP_HEIGHT / JUMP_DURATION
	WALL_JUMP_VELOCITY = JUMP_IMPULSE \
			* WALL_JUMP_VELOCITY_TO_JUMP_IMPULSE
	NEUTRAL_WALL_JUMP_VELOCITY = JUMP_IMPULSE \
			* NEUTRAL_WALL_JUMP_VELOCITY_TO_JUMP_IMPULSE
	WALL_GRAB_JUMP_VELOCITY = JUMP_IMPULSE \
			* WALL_GRAB_JUMP_VELOCITY_TO_JUMP_IMPULSE
	MAX_FALL_SPEED = MAX_FALL_SPEED_TO_JUMP_IMPULSE \
			* JUMP_IMPULSE
	MAX_WALL_FALL_SPEED = MAX_WALL_FALL_SPEED_TO_MAX_FALL_SPEED \
			* MAX_FALL_SPEED
	ACCELERATION = ACCELERATION_TO_MAX_SPEED_H \
			* MAX_SPEED_H
	LOW_ACCELERATION = LOW_ACCELERATION_TO_ACCELERATION \
			* ACCELERATION
	DASH_MIN_SPEED = DASH_MIN_SPEED_TO_JUMP_IMPULSE \
			* JUMP_IMPULSE
	WAVE_DASH_VERTICAL_IMPULSE = WAVE_DASH_VERTICAL_IMPULSE_TO_JUMP_IMPULSE \
			* JUMP_IMPULSE
	GRUMP_IMPULSE = GRUMP_IMPULSE_TO_JUMP_IMPULSE \
			* JUMP_IMPULSE


func _dash_move_and_slide(delta):
	var has_collided = false
	var tot_distance = delta * velocity.length()
	var ms = max_slides
	var speed = velocity.length()
	max_slides = 1
	for i in range(ms):
		if move_and_slide():
			has_collided = true
			var col = get_slide_collision(get_slide_collision_count() - 1)
			var rem = col.get_remainder().length()
			if is_zero_approx(rem):
				velocity = speed * velocity.normalized()
				break
			velocity = rem / (tot_distance) \
					* speed \
					* velocity.normalized()
		else:
			break
	velocity = speed * velocity.normalized()
	max_slides = ms
	return has_collided


func _death(_body):
	# menu.open(menu.Kind.DEATH)
	get_tree().reload_current_scene()
