class_name PlayerAir
extends PlayerBaseState


func unhandled_input(event):
	if super(event):
		return true
	# Grab
	elif (
			event.is_action_pressed("grab")
			and grab_area.has_overlapping_bodies()
			and grab_area.get_node("CantGrab").is_stopped()
	):
		transitioned.emit(self, "GrabWall")
		return true
	# Wall jump
	elif event.is_action_pressed("jump") and (
			wall_jump_detectors.is_detecting()
			or not wall_coyote_fc.is_stopped()
	):
		transitioned.emit(self, "WallJump")
		return true
	# Jump buffers
	elif event.is_action_pressed("jump"):
		jump_buffer_fc.start()
		wall_jump_buffer_fc.start()
	
	return false


func check_transition():
	# Jump when touching wall with the jump buffer
	if (
			not wall_jump_buffer_fc.is_stopped()
			and wall_jump_detectors.is_detecting()
	):
		transitioned.emit(self, "WallJump")
	else:
		super()


func physics_process(delta):
	super(delta)
	
	setup_wall_jump()


func enter(msg = {}):
	super(msg)
	
	setup_wall_jump()


func head_bump(delta):
	if player.velocity.y >= 0:
		return
	head_bump_corner_detectors.update_detectors(delta * player.velocity)
	var corner_dir_pos = head_bump_corner_detectors.get_corner_dir_pos()
	if corner_dir_pos == null:
		return
	var corner_pos = corner_dir_pos[1]
	var p_corner_pos = get_player_corner_pos(corner_dir_pos[0])
	var offset = (
			corner_pos \
			- projection(corner_pos, p_corner_pos, player.velocity)
	)
	player.position += offset \
			+ player.corner_dodge_offset_margin * offset.normalized()
