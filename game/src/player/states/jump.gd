class_name PlayerJump
extends PlayerJumps


var _just_entered = false
var _hold_action = "jump"


func unhandled_input(event):
	if super(event):
		return true
	# First jump peak
	elif event.is_action_released(_hold_action):
		transitioned.emit(self, "AirPeak")
		return true
	
	return false


func check_transition():
	# Fall
	if player.velocity.y >= 0:
		transitioned.emit(self, "JumpFall")
	else:
		super()


func physics_process(delta):
	super(delta)
	
	if sm.input_dir.x:
		player.set_direction_h(sm.input_dir.x)
	update_velocity_h(sm.input_dir.x, delta, false)
	
	head_bump(delta)
	
	if not _just_entered:
		player.velocity.y += delta * player.GRAVITY_UP
	else:
		_just_entered = false


func enter(msg = {}):
	super(msg)
	
	if msg.has("from_grump"):
		_hold_action = "grump"
	else:
		_hold_action = "jump"
	
	if msg.has("no_impulse"):
		_just_entered = false
		return
	_just_entered = true
	
	# Booleans (second jump, can dash) and animation
	if msg.has("from") and msg.from == "Air":
		sm.has_second_jump = false
		player.find_child("Animations").play("double_jump")
	else:
		sm.can_dash = true
		sm.has_second_jump = true
		stretch()
	
	# Velocity and effects
	if not msg.has("wave_dash"):
		player.velocity.y = - player.JUMP_IMPULSE
		if msg.has("from") and msg.from == "Air":
			player.create_jump_effect(sm.input_dir.x, true)
		else:
			player.create_jump_effect(sm.input_dir.x)
		return
	if msg["wave_dash"] == "horizontal":
		player.velocity = (
					player.WAVE_DASH_SPEED_MULTIPLIER \
					* Vector2(sm.input_dir.x * player.velocity.length(), 0)
				) \
				- Vector2(0, player.WAVE_DASH_VERTICAL_IMPULSE)
		player.create_jump_effect(sm.input_dir.x, false, true)
	elif msg["wave_dash"] == "vertical":
		player.velocity = player.velocity.length() \
				* player.NEUTRAL_WAVE_DASH_VECTOR
		player.create_jump_effect(0, false, true)


func stretch():
	var scale_h = 0.6
	player.find_child("Sprite").scale = Vector2(scale_h, 1.0/scale_h)
	var scale_tween = get_tree().create_tween()
	scale_tween.tween_property(
			player.find_child("Sprite"),
			"scale",
			Vector2.ONE,
			0.2
	)
