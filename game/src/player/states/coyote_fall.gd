class_name PlayerCoyoteFall
extends PlayerFalls


func unhandled_input(event):
	if super(event):
		return true
	# Coyote jump
	if event.is_action_pressed("jump"):
		transitioned.emit(self, "Jump", {jump = "coyote"})
		return true
	
	return false


func check_transition():
	if coyote_fc.is_stopped():
		transitioned.emit(self, "JumpFall")
		return true
	else:
		super()


func enter(msg = {}):
	super(msg)
	
	coyote_fc.start()


func exit():
	super()
	
	coyote_fc.stop()
