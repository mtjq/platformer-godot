class_name PlayerBaseState
extends Node


signal transitioned

@export var anim_name: String

var player

@onready var jump_buffer_fc = owner.find_child("JumpBufferFC")
@onready var coyote_fc = owner.find_child("CoyoteFC")
@onready var wall_jump_buffer_fc = owner.find_child("WallJumpBufferFC")
@onready var wall_coyote_fc = owner.find_child("WallCoyoteFC")
@onready var wall_jump_const_vel_fc = owner.find_child("WallJumpConstVelFC")
@onready var neutral_wall_jump_const_vel_fc = \
		owner.find_child("NeutralWallJumpConstVelFC")
@onready var low_acc_fc = owner.find_child("LowAccFC")
@onready var dash_freeze_fc = owner.find_child("DashFreezeFC")
@onready var dash_fc = owner.find_child("DashFC")
@onready var wave_dash_buffer_fc = owner.find_child("WaveDashBufferFC")
@onready var grump_prepare_fc = owner.find_child("GrumpPrepareFC")
@onready var grump_const_vel_fc = owner.find_child("GrumpConstVelFC")
@onready var sprite = owner.find_child("Sprite")
@onready var grab_area = owner.find_child("GrabArea")
@onready var wall_jump_detectors = owner.find_child("WallDetectors")
@onready var head_bump_corner_detectors = \
		owner.find_child("HeadBumpCornerDetectors")
@onready var grump_corner_detectors = owner.find_child("GrumpCornerDetectors")
@onready var grump_area = owner.find_child("GrumpArea")
@onready var sm = owner.find_child("StateMachine")


func _ready():
	pass


func input(_event):
	pass


func unhandled_input(event):
	# Dash
	if event.is_action_pressed("dash") and sm.can_dash:
		var msg = {no_second_jump = true} if "Second" in name else {}
		transitioned.emit(self, "Dash", msg)
		return true
	# Grump
	elif (
			event.is_action_pressed("grump")
			and grump_area.has_overlapping_areas()
	):
		transitioned.emit(self, "Grump")
		return true
	
	return false


func check_transition():
	pass


func process(_delta):
	pass


func physics_process(_delta):
	pass


func enter(_msg):
	sm.play_anim(anim_name)


func exit():
	pass


func update_velocity_h(dir, delta, cap = true):
	var acc = player.ACCELERATION if low_acc_fc.is_stopped() \
			else player.LOW_ACCELERATION
	if cap or dir != sign(player.velocity.x):
		player.velocity.x = move_toward(
				player.velocity.x,
				dir * player.MAX_SPEED_H,
				delta * acc
		)
	else:
		player.velocity.x = move_toward(
				player.velocity.x,
				dir * max(abs(player.velocity.x), player.MAX_SPEED_H),
				delta * acc
		)


func setup_wall_jump():
	if wall_jump_detectors.is_detecting():
		wall_coyote_fc.start()
		player.wall_jump_dir = - wall_jump_detectors.get_wall_dir()
		player.wall_jump_pos = Vector2(
				wall_jump_detectors.get_contact_x(),
				player.position.y
		)


func projection(to_project, line_origin, line_dir):
	if is_zero_approx(line_dir.y):
		return Vector2(to_project.x, line_origin.y)
	var y = 1.0 / line_dir.length_squared() \
			* (line_origin.y * (line_dir.x ** 2) \
				+ to_project.y * (line_dir.y ** 2) \
				+ (to_project.x - line_origin.x) \
					* line_dir.x * line_dir.y
			)
	var x = line_origin.x + \
			(y - line_origin.y) \
					* line_dir.x / line_dir.y
	return Vector2(x, y)


func get_player_corner_pos(corner_dir):
	return (
			player.position \
			+ player.player_size * Vector2(0.5, 1) \
				* (corner_dir + Vector2(corner_dir.x, -1)) / 2.0
	)


func corner_boost():
	var corner_dir_pos = grump_corner_detectors.get_corner_dir_pos()
	if corner_dir_pos == null:
		return false
	if not (
			sm.input_dir.y < 0
			and sm.input_dir.dot(corner_dir_pos[0]) == 0
			and player.velocity.normalized().dot(sm.input_dir.normalized()) \
					> sqrt(0.5)
	):
		return false
	player.position = corner_dir_pos[1] \
			- (player.player_size / 2 \
				* (corner_dir_pos + Vector2(corner_dir_pos[0].x, -1)) / 2)
	player.velocity = .85 * player.JUMP_IMPULSE * sm.input_dir.normalized()
	return true
