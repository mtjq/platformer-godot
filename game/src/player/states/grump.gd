class_name PlayerGrump
extends PlayerLockedJumps


@export var use_snap = true
@export var use_pause = true

var _initial_speed = 0.0
var _initial_dir = Vector2(0.0, 0.0)
var _impulse_created = false


func _ready():
	super()
	await owner.ready
	if not use_pause:
		grump_prepare_fc.over.connect(_create_impulse)


func check_transition():
	# Fall
	if _impulse_created and grump_const_vel_fc.is_stopped():
		if Input.is_action_pressed("grump"):
			transitioned.emit(
					self,
					"Jump",
					{no_impulse = true, from_grump = true}
			)
			return true
		else:
			transitioned.emit(self, "AirPeak")
			return true
	else:
		super()


func physics_process(delta):
	super(delta)
	
	if (
			use_pause
			and not _impulse_created
			and not Input.is_action_pressed("grump")
	):
		_create_impulse()
		return


func enter(msg = {}):
	super(msg)
	
	_impulse_created = false
	_initial_speed = max(player.velocity.length(), player.GRUMP_IMPULSE)
	_initial_dir = player.velocity.normalized()
	
	player.velocity = Vector2.ZERO
	var grumpers = grump_area.get_overlapping_areas()
	var grumper = grumpers[0]
	var dist_to_grumper = \
			grumper.position.distance_to(player.get_central_pos())
	for g in grumpers:
		if g.position.distance_to(player.get_central_pos()) \
				< dist_to_grumper:
			grumper = g
			dist_to_grumper = \
					g.position.distance_to(player.get_central_pos())
	if not use_snap:
		if not use_pause:
			_create_impulse()
		return
	
	var pos_tween = get_tree().create_tween()
	var prep_dur = dist_to_grumper / _initial_speed
	pos_tween.tween_property(player, "position", grumper.position, prep_dur)\
			.set_trans(Tween.TRANS_BOUNCE)
	grump_prepare_fc.start(ceil(prep_dur * Engine.physics_ticks_per_second))


func exit():
	super()
	
	_impulse_created = false
	grump_const_vel_fc.stop()
	grump_prepare_fc.stop()


func _create_impulse():
	_impulse_created = true
	grump_const_vel_fc.start()
	var change_direction_angle = _initial_dir.angle_to(sm.input_dir)
	var speed_mult = (- abs(change_direction_angle) + PI) / PI \
			* (player.GRUMP_SPEED_MULT_MAX - player.GRUMP_SPEED_MULT_MIN) \
			+ player.GRUMP_SPEED_MULT_MIN
	var impulse = max(speed_mult * _initial_speed, player.GRUMP_IMPULSE)
	player.velocity = impulse * sm.input_dir.normalized()
