class_name PlayerGround
extends PlayerBaseState


func unhandled_input(event):
	if super(event):
		return true
	# Jump
	elif event.is_action_pressed("jump"):
		transitioned.emit(self, "Jump")
		return true
	
	return false


func check_transition():
	# Fall
	if not owner.is_on_floor():
		transitioned.emit(self, "CoyoteFall")
	else:
		super()


func enter(msg = {}):
	super(msg)
	
	low_acc_fc.stop()
	wall_coyote_fc.stop()
	jump_buffer_fc.stop()
	wall_jump_buffer_fc.stop()
	
	sm.can_dash = true
	sm.has_second_jump = true
	
	if not msg.has("from"):
		return
	elif msg["from"] == "Air":
		player.create_land_dust()
