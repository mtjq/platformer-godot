class_name PlayerAirPeak
extends PlayerAirFree


func unhandled_input(event):
	if super(event):
		return true
	# Second jump
	if event.is_action_pressed("jump") and sm.has_second_jump:
		transitioned.emit(self, "Jump", {from = "Air"})
		return true
	
	return false


func check_transition():
	if player.velocity.y >= 0:
		transitioned.emit(self, "JumpFall")
		return true
	else:
		super()


func physics_process(delta):
	super(delta)
	
	player.velocity.y *= 0.7
	player.velocity.y = move_toward(
			player.velocity.y,
			player.MAX_FALL_SPEED,
			delta * player.GRAVITY_DOWN
	)
