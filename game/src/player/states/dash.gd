class_name PlayerDash
extends PlayerBaseState


var AfterImage = preload("res://game/src/effects/after_image.tscn")
var dash_corner_detectors
var _can_wave_dash = true
var _has_no_contact = false
var _after_image_colour = Color.WHITE
var _pre_dash_speed = 0.0


func _ready():
	super()
	await owner.ready
	dash_corner_detectors = player.find_child("DashBumpCornerDetectors")
	dash_freeze_fc.over.connect(_create_impulse)


func unhandled_input(event):
	if not dash_freeze_fc.is_stopped():
		return
	
	if super(event):
		return true
	# Wave dash
	if _can_wave_dash and event.is_action_pressed("jump"):
		if _wave_dash():
			return true
	# Wave dash buffer
	if event.is_action_pressed("jump") and not player.is_on_floor():
		wave_dash_buffer_fc.start()
	# Jump while dashing
	elif event.is_action_pressed("jump") and player.is_on_floor():
		player.velocity.y = - player.JUMP_IMPULSE
	
	return false


func check_transition():
	if not dash_freeze_fc.is_stopped():
		return
	
	# Natural end
	if dash_fc.is_stopped():
		player.velocity.x = clamp(
				player.velocity.x,
				- player.MAX_SPEED_H,
				player.MAX_SPEED_H
		)
		if player.is_on_floor():
			if player.velocity.x == 0.:
				transitioned.emit(self, "Idle", {from = "Dash"})
				return true
			else:
				transitioned.emit(self, "Run", {from = "Dash"})
				return true
		if (
				Input.is_action_pressed("grab")
				and grab_area.has_overlapping_bodies()
		):
			transitioned.emit(self, "GrabWall")
			return true
		transitioned.emit(self, "AirPeak")
		return true
	# Wave dash with buffer
	if _can_wave_dash and not wave_dash_buffer_fc.is_stopped():
		if _wave_dash():
			return true
	super()


func physics_process(delta):
	if not dash_freeze_fc.is_stopped():
		return
	
	if sm.input_dir.x:
		player.set_direction_h(sm.input_dir.x)
	
	_dodge_corner(delta)
	
	if not player.is_on_surface():
		_can_wave_dash = true
		_has_no_contact = true
	elif _has_no_contact:
		_has_no_contact = false
		dash_fc.add_frames(player.SURFACE_SLIDE_ADDITIONAL_FRAMES)
	
	var frames_past = dash_fc.get_total_frames() - dash_fc.get_frames_left()
	if frames_past % 2 == 0:
		_create_after_image()
	
	setup_wall_jump()


func enter(msg = {}):
	super(msg)
	
	low_acc_fc.stop()
	wall_coyote_fc.stop()
	jump_buffer_fc.stop()
	wall_jump_buffer_fc.stop()
	
	sm.can_dash = false
	
	randomize()
	_after_image_colour = Color.from_hsv(randf(), 1, 1, 1)
	_has_no_contact = not player.is_on_surface()
	_can_wave_dash = _has_no_contact
	
	_pre_dash_speed = player.velocity.length()
	player.velocity = Vector2.ZERO
	dash_freeze_fc.start()
	
	setup_wall_jump()


func exit():
	dash_fc.stop()
	wave_dash_buffer_fc.stop()
	low_acc_fc.start()


func _create_impulse():
	var dash_dir = sm.input_dir if sm.input_dir \
			else Vector2(player.direction_h, 0.0)
	player.velocity = max(
			player.DASH_SPEED_MULTIPLIER * _pre_dash_speed,
			player.DASH_MIN_SPEED
	) \
			* dash_dir.normalized()
	dash_fc.start()


func _dodge_corner(delta):
	dash_corner_detectors.update_detectors(delta * player.velocity)
	var corner_dir_pos = dash_corner_detectors.get_corner_dir_pos()
	if corner_dir_pos == null:
		return
	var corner_dir = corner_dir_pos[0]
	if corner_dir.y > 0 and not wave_dash_buffer_fc.is_stopped():
		return
	var corner_pos = corner_dir_pos[1]
	var corner_to_vel_angle = corner_dir.angle_to(player.velocity)
	if (
			abs(corner_to_vel_angle) < PI / 4.0
			or abs(corner_to_vel_angle) >= 3.0 * PI / 4.0
	):
		return
	var p_corner_pos = get_player_corner_pos(corner_dir)
	var offset = (
			corner_pos \
			- projection(corner_pos, p_corner_pos, player.velocity)
	)
	player.position += offset \
			+ player.corner_dodge_offset_margin * offset.normalized()


func _create_after_image():
	var after_image = AfterImage.instantiate()
	after_image.position = player.position
	after_image.set_texture(
			player.get_current_texture(),
			player.find_child("Sprite").flip_h
	)
	var base_r = pow(
			float(dash_fc.get_frames_left()) \
					/ float(dash_fc.get_total_frames()),
			1.0
	) * 0.9
	after_image.set_starting_material(_after_image_colour, base_r)
	get_tree().current_scene.add_child(after_image)


func _wave_dash():
	# Ground wave dash
	if player.is_on_floor() and player.velocity.x != 0:
		var jump_msg = "vertical" if sm.input_dir.x == 0 else "horizontal"
		transitioned.emit(self, "Jump", {wave_dash = jump_msg})
		return true
	# Wall wave dash
	if (
			wall_jump_detectors.is_detecting()
			and player.velocity.y != 0
	):
		var msg = {dash = true} if player.velocity.y < 0 else {}
		transitioned.emit(self, "WallJump", msg)
		return true
	return false
