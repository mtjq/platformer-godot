class_name PlayerFalls
extends PlayerAirFree


func unhandled_input(event):
	if super(event):
		return true
	# Bunny hop
	if event.is_action_pressed("jump") and player.is_on_floor():
		transitioned.emit(self, "Jump", {bunny = true})
		return true
	
	return false


func check_transition():
	# Touching floor
	if player.is_on_floor():
		if not jump_buffer_fc.is_stopped():
			transitioned.emit(self, "Jump", {bunny = true})
			return true
		elif is_zero_approx(player.velocity.x) and sm.input_dir.x == 0:
			transitioned.emit(self, "Idle", {from = "Air"})
			return true
		else:
			transitioned.emit(self, "Run", {from = "Air"})
			return true
	else:
		super()


func physics_process(delta):
	super(delta)
	
	if grab_area.has_overlapping_bodies() and sm.input_dir.x:
		player.velocity.y = move_toward(
				player.velocity.y,
				player.MAX_WALL_FALL_SPEED,
				delta * player.GRAVITY_DOWN
		)
		player.velocity.y = min(player.velocity.y, player.MAX_WALL_FALL_SPEED)
	else:
		player.velocity.y = move_toward(
				player.velocity.y,
				player.MAX_FALL_SPEED,
				delta * player.GRAVITY_DOWN
		)
