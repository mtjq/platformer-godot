class_name PlayerGrabWall
extends PlayerBaseState


func _ready():
	super()


func unhandled_input(event):
	if super(event):
		return true
	# Ungrab
	elif event.is_action_released("grab"):
		if not player.is_on_floor():
			transitioned.emit(self, "Fall")
			return true
		else:
			if sm.input_dir.x:
				transitioned.emit(self, "Run", {from = "Grab"})
				return true
			else:
				transitioned.emit(self, "Idle", {from = "Grab"})
				return true
	# elif sm.actions_just_pressed.jump:
	# 	grab_area.get_node("CantGrab").start()
	# 	transitioned.emit(self, "Jump", {jump = "grab"})
	
	return false


func enter(msg = {}):
	super(msg)

	low_acc_fc.stop()
	wall_coyote_fc.stop()
	jump_buffer_fc.stop()
	wall_jump_buffer_fc.stop()
	
	owner.velocity = Vector2.ZERO
