class_name PlayerJumps
extends PlayerAir


func enter(msg = {}):
	super(msg)
	
	jump_buffer_fc.stop()
	wall_jump_buffer_fc.stop()
	wall_coyote_fc.stop()
	low_acc_fc.stop()
