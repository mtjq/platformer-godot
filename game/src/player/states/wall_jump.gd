class_name PlayerWallJump
extends PlayerLockedJumps


func check_transition():
	# Fall
	if wall_jump_const_vel_fc.is_stopped():
		if Input.is_action_pressed("jump"):
			transitioned.emit(self, "Jump", {no_impulse = true})
			return true
		else:
			transitioned.emit(self, "AirPeak")
			return true
	else:
		super()


func enter(msg = {}):
	super(msg)
	
	_wall_jump(sm.input_dir.x == 0 and sm.input_dir.y < 0, msg.has("dash"))


func exit():
	super()
	
	wall_jump_const_vel_fc.stop()
	neutral_wall_jump_const_vel_fc.stop()


func _wall_jump(neutral = false, wave_dash = false):
	sm.can_dash = true
	if wave_dash:
		var base_vec = player.NEUTRAL_WALL_WAVE_DASH_VECTOR if neutral \
				else player.WALL_WAVE_DASH_VECTOR
		player.velocity = player.velocity.length() * \
				base_vec * Vector2(player.wall_jump_dir, 1)
	else:
		if neutral:
			player.velocity = player.NEUTRAL_WALL_JUMP_VELOCITY \
					* Vector2(player.wall_jump_dir, 1)
		else:
			player.velocity = player.WALL_JUMP_VELOCITY \
					* Vector2(player.wall_jump_dir, 1)
	if neutral:
		neutral_wall_jump_const_vel_fc.start()
	else:
		wall_jump_const_vel_fc.start()
	player.create_wall_jump_effect(neutral)
