class_name PlayerJumpFall
extends PlayerFalls


func unhandled_input(event):
	if super(event):
		return true
	# Second jump
	if event.is_action_pressed("jump") and sm.has_second_jump:
		sm.has_second_jump = false
		transitioned.emit(self, "Jump", {from = "Air"})
		return true
	
	return false
