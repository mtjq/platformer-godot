extends Node


@export var initial_state := NodePath()

var states = {}
var input_dir = Vector2.ZERO
var can_dash = true
var has_second_jump = true

@onready var state = get_node(initial_state)


func init_children(node):
	if node.get_child_count() == 0 and node is PlayerBaseState:
		node.transitioned.connect(on_child_transition)
		node.player = owner as Player
		states[node.name] = node
		return
	for child in node.get_children():
			init_children(child)


func init():
	for child in get_children():
		init_children(child)
	state.enter()


func sm_input(event):
	if state:
		state.input(event)


func sm_unhandled_input(event):
	if state:
		state.unhandled_input(event)


func sm_process(delta):
	if state:
		state.process(delta)


func sm_physics_process(delta):
	_pre_process()
	if state:
		state.physics_process(delta)
		state.check_transition()


func on_child_transition(emitting_state, target_state_name, msg = {}):
	if emitting_state != state or not states.has(target_state_name):
		return
	
	if state:
		state.exit()
	var target_state = states[target_state_name]
	target_state.enter(msg)
	state = target_state


func _pre_process():
	input_dir = Vector2(
			Input.get_axis("left", "right"),
			Input.get_axis("up", "down")
	).sign()


func play_anim(anim):
	owner.find_child("Animations").play(anim)
