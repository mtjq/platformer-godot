class_name PlayerGrumpPause
extends PlayerAir


var _initial_velocity


func _ready():
	super()
	await owner.ready
	grump_prepare_fc.over.connect(_impulse)
	grump_const_vel_fc.over.connect(low_acc_fc.start)


func process_transition(_delta):
	if not grump_prepare_fc.is_stopped():
		return
	
	# Grab
	if (
			Input.is_action_pressed("grab")
			and grab_area.has_overlapping_bodies()
			and grab_area.get_node("CantGrab").is_stopped()
	):
		transitioned.emit(self, "GrabWall")
		return
	# Dash
	if sm.actions_just_pressed.dash and sm.can_dash:
		transitioned.emit(self, "Dash", {from = "Air"})
		return
	# TODO: corner boost?
	# Fall
	if (
			grump_const_vel_fc.is_stopped()
			and (player.velocity.y >= 0 or not Input.is_action_pressed("grump"))
	):
		transitioned.emit(self, "Fall")
		return


func process_physics(delta):
	if not (grump_prepare_fc.is_stopped() and grump_const_vel_fc.is_stopped()):
		return
	
	if sm.input_dir.x:
		player.set_direction_h(sm.input_dir.x)
	
	if Input.is_action_pressed("grump"):
		head_bump(delta)


func enter(msg = {}):
	super(msg)
	
	_consume_grump()
	player.velocity = Vector2.ZERO
	var grumper = grump_area.get_overlapping_areas()[0]
	var pos_tween = get_tree().create_tween()
	var tween_dur = float(grump_prepare_fc.get_total_frames() - 2) \
			/ float(Engine.physics_ticks_per_second)
	pos_tween.tween_property(player, "position", grumper.position, tween_dur)\
			.set_trans(Tween.TRANS_EXPO)
	_initial_velocity = player.velocity
	grump_prepare_fc.start()


func exit():
	wall_jump_const_vel_fc.stop()
	low_acc_fc.stop()


func _consume_grump():
	sm.actions_just_pressed.grump = false


func _impulse():
	grump_const_vel_fc.start()
	sm.can_dash = true
	player.velocity = player.GRUMP_IMPULSE * sm.input_dir.normalized()
