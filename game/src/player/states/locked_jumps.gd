class_name PlayerLockedJumps
extends PlayerJumps


func physics_process(delta):
	super(delta)
	
	if sm.input_dir.x:
		player.set_direction_h(sm.input_dir.x)
	
	head_bump(delta)


func enter(msg = {}):
	super(msg)
	
	sm.can_dash = true
	sm.has_second_jump = true


func exit():
	low_acc_fc.start()
