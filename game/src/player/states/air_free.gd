class_name PlayerAirFree
extends PlayerAir


func physics_process(delta):
	super(delta)
	
	if sm.input_dir.x:
		player.set_direction_h(sm.input_dir.x)
	
	# Update velocity
	update_velocity_h(sm.input_dir.x, delta, false)


func enter(msg = {}):
	if not player.find_child("Animations").current_animation == "double_jump":
		super(msg)
