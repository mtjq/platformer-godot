class_name PlayerRun
extends PlayerGround


func check_transition():
	# Idle
	if is_zero_approx(owner.velocity.x) and sm.input_dir.x == 0:
		transitioned.emit(self, "Idle")
	else:
		super()


func physics_process(delta):
	super(delta)
	
	update_velocity_h(sm.input_dir.x, delta)
	if sm.input_dir.x:
		if sm.input_dir.x != player.direction_h:
			player.create_run_dust()
		player.set_direction_h(sm.input_dir.x)


func enter(msg = {}):
	super(msg)
	
	if msg.has("from") and msg["from"] == "Dash":
		player.velocity = min(player.MAX_SPEED_H, player.velocity.length()) \
				* player.velocity.normalized()
