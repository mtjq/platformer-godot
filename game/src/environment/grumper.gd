class_name Grump
extends Area2D


func _ready():
	area_entered.connect(_on_area_entered)
	area_exited.connect(_on_area_exited)


func _process(_delta):
	pass


func _on_area_entered(_b):
	$Sprite.scale = Vector2(2, 2)


func _on_area_exited(_b):
	$Sprite.scale = Vector2(1, 1)
