# Description

This is a project that aims at experimenting with any standard 2D platformer 
mechanics, and creating the best game feeling possible, without going as far as 
building a whole game or even whole levels. All art has been made be myself 
(though hugely inspired by Scourge Bringer). This is still a work in progress.

List of implemented mechanics:

- jump stops when you release the jump key
- falling gravity is bigger that jumping gravity
- coyote time
- jump buffer
- double jump
- wall jump (normal and vertical)
- dash
- wave dash (normal and vertical)
- wall jump wave dash (normal and vertical)
- shape casts that detect if the jumping character will hit the lower corners 
    of a platform, then helping it smoothly dodging it
- stretching effect on the character when jumping

# Trying it

Opening the project with Godot 4.1 and launching the main scene will open the 
game.

Press S/F for left/right movement, K to jump and J to dash.

![wall jump](resources/wall_jump.png)
